from transformers import AutoTokenizer, AutoModelForMaskedLM
import torch
import click


@click.command()
@click.option(
        "--dir",
        type=str,
        required=True,
        help="Path to txt file."
        )


def predict_sentences(sentence:str):
    # 前処理
    input = tokenizer.encode(sequence, return_tensors="pt")
    mask_token_index = torch.where(input == tokenizer.mask_token_id)[1]

    # 推論
    token_logits = model(input)[0]
    mask_token_logits = token_logits[0, mask_token_index, :]

    # 上位5トークンの出力
    top_5_tokens = torch.topk(mask_token_logits, 5, dim=1).indices[0].tolist()
    for token in top_5_tokens:
        print(tokenizer.decode([token]))

# txtの内容をList形式で出力
def read_txt(txt_path:str):
    with open(txt_path) as f:
        txts = f.read()
        txt_line = txts.split("\n")
    return txt_line

if __name__ == "__main__":
    tokenizer = AutoTokenizer.from_pretrained("bert-base-uncased")
    model = AutoModelForMaskedLM.from_pretrained("bert-base-uncased")
    
    txt_line = read_txt(dir)
    for txt in txt_line:
        sequence = txt.replace("[MASK]", tokenizer.mask_token)
        predict_sentences(sequence)
    
"""
    while True:
        inp = str(input("Input sentence: \n"))
        if "[MASK]" in inp:
            break
        else:
            print('The sentence need to have "[MASK]".')
    sequence = inp.replace("[MASK]", tokenizer.mask_token)
    predict_sentences(sequence)
"""
